package pl.mordesku.sda.facade.coffee.machine.frother;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BeanConfiguration {

    @Bean
    @Scope("sngleton")
    public MilkFrother milkFrother(){
        return new MilkFrother();
    }

}
