package pl.mordesku.sda.facade.coffee.machine.ingredients;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BeanConfiguration {

    @Bean
    @Scope("prototype")
    public MilkIngredient milkIngredient(){
        return new MilkIngredient();
    }

    @Bean
    @Scope("prototype")
    public WaterIngredient waterIngredient(){
        return new WaterIngredient(100);
    }
}
