package pl.mordesku.sda.facade.coffee.machine.grinder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BeanConfiguration {

    @Bean
    @Scope("singleton")
    public CoffeGrinder coffeGrinder(){
        return new CoffeGrinder();
    }

}
