package pl.mordesku.sda.facade.coffee.machine;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BeanConfiguration {

    @Bean
    @Scope("singleton")
    public AdvancedCoffeeMachine advancedCoffeeMachine(){
        return new AdvancedCoffeeMachine();
    }

    @Bean
    @Scope("singleton")
    public SimpleCoffeeMachineFacade simpleCoffeeMachineFacade(){
        return new SimpleCoffeeMachineFacade();
    }

}
